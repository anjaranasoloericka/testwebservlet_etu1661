RAZAFIMBAHINY Anjara Nasolo Ericka : ETU001661

Utilisation :

	* GenericFramework.jar dans le dossier jar de TestFramework

	* Création de classe (objet) :
		- Créer un constructeur sans argument
		- annoter la classe avec l'annotation @ClassAnnotation
		- création de méthode et attribution d'url sur la méthode avec annotation @UrlAnnotation(urlServlet=url du servlet) : 
			url du servlet de type String

	* Création de méthode :
		- Deux types de retour possible : ModelView ou void
		- Si return ModelView : 
			- Instancer votre ModelView (exemple : ModelView m=new ModelView())
			- Ajouter l'url de votre page jsp à ce ModelView (exemple : m.setUrl("listdept.jsp"));
			- Ajouter les données que vous voulez renvoyer sur la page jsp (exemple :m.addData("prenom", "Jean");
		- Si return void :
			- Exécution de fonction 

	* Création de formulaire :
		- Le nom du champ doit être égal à celui de la classe correspondante

	* Lancement 
		- url sur le navigateur = url de la méthode que vous voulez tester (url dans la méthode de la classe 
			que vous avez créer)

			- Si type de retour : ModelView : va dans la page JSP dans l'URL du ModelView et envoie les données correspondantes à cette page
			- Si type de retour : void : exécute la fonction correspondante à l'Url du servlet

		