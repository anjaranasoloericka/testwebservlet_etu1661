<%@page import="classeTest.Departement"%>
<%@page import="java.util.*"%>
<%
    ArrayList<Departement>alldept=(ArrayList<Departement>)request.getAttribute("alldept");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Framework</title>
    </head>
    <body>
        <h3>Liste departement</h3>
        <table border="1">
            <tr>
                <th>Id</th>
                <th>Nom</th>
            </tr>
            <% for(Departement dept:alldept){%>
            <tr>
                <td><%= dept.getId()%></td>
                <td><%= dept.getNom()%></td>
            </tr>
            <% }%>
        </table>
    </body>
</html>
