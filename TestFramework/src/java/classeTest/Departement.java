/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classeTest;

import annotation.*;
import java.util.ArrayList;
import utils.ModelView;

@ClassAnnotation
public class Departement {
    
    private int id;
    private String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Departement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Departement() {
    }
    
    
    
    @UrlAnnotation(urlServlet="see.do")
    public ModelView seeDepartement(){
        ModelView md=new ModelView();
        Departement dept=this;
        md.setUrl("seeDepartement.jsp");
        md.addData("dept", dept);
        return md;     
    }
    
    
    @UrlAnnotation(urlServlet="listdept.do")
    public ModelView getListDepartement(){
        ArrayList<Departement>array=new ArrayList<Departement>();
        array.add(new Departement(1,"Finance"));
        array.add(new Departement(2,"Comptabilite"));
        array.add(new Departement(3,"Ressources Humaines"));
        array.add(new Departement(4,"IT"));
        ModelView md=new ModelView();
        md.setUrl("listdept.jsp");
        md.addData("alldept", array);
        return md;
    }
    
    
    
    
    
}
